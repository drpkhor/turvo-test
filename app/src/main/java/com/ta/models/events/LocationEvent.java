package com.ta.models.events;

import com.ta.models.respmodels.Location;

import java.util.List;

/**
 * Created by rahil on 29/5/17.
 */

public class LocationEvent {


    public LocationEvent(List<Location> location) {
        this.location = location;
    }

    List<Location> location;

    public List<Location> getLocation() {
        return location;
    }

    public void setLocation(List<Location> location) {
        this.location = location;
    }
}
