package com.ta.models.events;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rahil on 29/5/17.
 */

public class ZoomToLatLngEvent {

    LatLng latLng;

    public ZoomToLatLngEvent(LatLng latLng) {
        this.latLng = latLng;
    }

    public LatLng getLatLng() {
        return latLng;
    }
}
