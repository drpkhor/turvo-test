package com.ta.models.respmodels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rahil on 29/5/17.
 */

public class Location implements Serializable {


    /**
     * formatted_address : 529 Kent Street, Sydney NSW, Australia
     * geometry : {"location":{"lat":-33.875046,"lng":151.205272}}
     * icon : http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png
     * id : 827f1ac561d72ec25897df088199315f7cbbc8ed
     * name : Tetsuya's
     * rating : 4.3
     * reference : CnRmAAAAmmm3dlSVT3E7rIvwQ0lHBA4sayvxWEc4nZaXSSjRtfKRGoYnfr3d5AvQGk4e0u3oOErXsIJwtd3Wck1Onyw6pCzr8swW4E7dZ6wP4dV6AsXPvodwdVyqHgyGE_K8DqSp5McW_nFcci_-1jXb5Phv-RIQTzv5BjIGS0ufgTslfC6dqBoU7tw8NKUDHg28bPJlL0vGVWVgbTg
     * types : ["restaurant","food","establishment"]
     */

    private String formatted_address;
    private GeometryEntity geometry;
    private String icon;
    private String id;
    private String name;
    private double rating;
    private String reference;
    private List<String> types;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public GeometryEntity getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryEntity geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public static class GeometryEntity implements  Serializable{
        /**
         * location : {"lat":-33.875046,"lng":151.205272}
         */

        private LocationEntity location;

        public LocationEntity getLocation() {
            return location;
        }

        public void setLocation(LocationEntity location) {
            this.location = location;
        }

        public static class LocationEntity  implements  Serializable{
            /**
             * lat : -33.875046
             * lng : 151.205272
             */

            private double lat;
            private double lng;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public void setLng(double lng) {
                this.lng = lng;
            }
        }
    }
}
