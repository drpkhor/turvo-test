package com.ta.activities;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import com.ta.R;
import com.ta.SmartFragmentStatePagerAdapter;
import com.ta.api.ApiConstants;
import com.ta.base.BaseToolBarActivity;
import com.ta.databinding.ActivityMainBinding;
import com.ta.fragment.MapFragment;
import com.ta.fragment.PlaceListFragment;
import com.ta.models.events.LocationEvent;
import com.ta.models.general.ApiResp;
import com.ta.models.respmodels.Location;
import com.ta.utils.AppUtils;
import com.ta.utils.DialogUtil;
import com.ta.utils.Net;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseToolBarActivity {

    private ActivityMainBinding mBinding;
    private SmartFragmentStatePagerAdapter mPagerAdapter;
    private View.OnClickListener mRetryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callGetLocationApi();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initializeUi(ViewDataBinding binding) {

        setUpToolbar(R.string.Home, 0);
        mBinding = (ActivityMainBinding) binding;

        mBinding.toolBar.tabLayout.setupWithViewPager(mBinding.content.pager);
        mBinding.content.pager.setAdapter(createAdapter());
        callGetLocationApi();
    }

    private void callGetLocationApi() {

        if (!Net.isConnected(mBinding.content.pager, mRetryListener)) return;

        Call<ApiResp<List<Location>>> call = mApis.getLocations(ApiConstants.GIST_URL);
        showProgressDialog();
        AppUtils.enqueueCall(call, new Callback<ApiResp<List<Location>>>() {
            @Override
            public void onResponse(Call<ApiResp<List<Location>>> call, Response<ApiResp<List<Location>>> response) {
                hideProgressDialog();
                EventBus.getDefault().post(new LocationEvent(response.body().getResult()));
            }

            @Override
            public void onFailure(Call<ApiResp<List<Location>>> call, Throwable t) {
                hideProgressDialog();
                DialogUtil.showTryAgainSnackBar(mBinding.content.pager, mRetryListener);
            }
        });
    }

    private PagerAdapter createAdapter() {
        return mPagerAdapter = new SmartFragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                return position == 0 ? PlaceListFragment.newInstance() : MapFragment.newInstance();
            }

            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position == 0 ? getString(R.string.list_view) : getString(R.string.map_view);
            }
        };
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PlaceListFragment.REQ_CODE_DETAILS){
            //MapFragment fragment = (MapFragment) mPagerAdapter.getRegisteredFragment(1);
            mBinding.content.pager.setCurrentItem(1);
        }
    }
}
