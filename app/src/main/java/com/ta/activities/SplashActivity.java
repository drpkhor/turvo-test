package com.ta.activities;

import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.crash.FirebaseCrash;
import com.ta.R;
import com.ta.base.BaseActivity;


public class SplashActivity extends BaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void initializeUi(ViewDataBinding binding) {

        FirebaseCrash.report(new Exception("My first Android non-fatal error"));
        startActivity(new Intent(this,MainActivity.class));
        finish();

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void onClick(View v) {

    }
}
