package com.ta.activities;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.ta.R;
import com.ta.base.BaseNavigationActivity;
import com.ta.databinding.ActivityHomeBinding;
import com.ta.fragment.HomeFragment;

public class HomeActivity extends BaseNavigationActivity {

    private ActivityHomeBinding mBinder;
    private HomeFragment mHomeFragment;
    private SCREENS mCurrentScreen;

    public enum SCREENS {
        HOME, MY_PROFILE,
        ABOUT_US, SETTINGS, PRIVACY_POLICY
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void initializeUi(ViewDataBinding binding) {
        mBinder = (ActivityHomeBinding) binding;

        //setting toolbar
        setUpToolBar("", R.menu.home, getToolbarItemClickListener());
        //setting navigation

        setupRightDrawer(R.id.layout_drawer, R.id.action_settings, new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        setupNavigation(R.id.layout_drawer);
        //pushing home fragment as default on home activity
        mHomeFragment = HomeFragment.newInstance();
        setFragment(mHomeFragment, false);

        //updating selected fragment to manage menu items accordingly
        updateSelectedFragment(SCREENS.HOME, getString(R.string.home));
    }


    private void updateSelectedFragment(SCREENS screen, String title) {
        mCurrentScreen = screen;
        mToolbar.setTitle(title);
    }


    /*This method is used to handle menu item click event*/
    private Toolbar.OnMenuItemClickListener getToolbarItemClickListener() {
        return new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    /*case R.id.action_sort:
                        startActivity(new Intent(mThis, SortActivity.class));
                        return true;*/

                }
                return false;
            }
        };
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            updateSelectedFragment(SCREENS.HOME, getString(R.string.home));
        } else {
            super.onBackPressed();
        }
    }
}
