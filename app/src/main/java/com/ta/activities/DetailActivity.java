package com.ta.activities;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ta.R;
import com.ta.base.BaseToolBarActivity;
import com.ta.databinding.ActivityDetailBinding;
import com.ta.fragment.WorkaroundMapFragment;
import com.ta.models.respmodels.Location;
import com.ta.utils.AppConstants;
import com.ta.utils.Lg;


public class DetailActivity extends BaseToolBarActivity implements OnMapReadyCallback {

    private WorkaroundMapFragment mMapFragment;
    private ActivityDetailBinding mBinding;
    private GoogleMap mMap;
    private Location mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocation = (Location) getIntent().getSerializableExtra(AppConstants.LOCATION_DATA_EXTRA);
        mBinding.content.tvHighLights.setText(TextUtils.join("\n", mLocation.getTypes()));
    }

    @Override
    public void initializeUi(ViewDataBinding binding) {

        setUpToolbar(getString(R.string.title_activity_detail));

        mBinding = (ActivityDetailBinding) binding;
        mMapFragment = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        mMapFragment.setScrollView(mBinding.content.scrollView);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_detail;
    }


    public Marker addMarker(LatLng latLng) {
        //final LatLng latLng = new LatLng(lat, lng);
        MarkerOptions mMarkerOptionsPin = new MarkerOptions().position(latLng);
        // mMarkerOptionsPin.icon(BitmapDescriptorFactory.fromResource(markerDrawableId));
        Marker marker = mMap.addMarker(mMarkerOptionsPin);

        Lg.d(TAG + "marker id", marker.getId());
        return marker;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        double lat = mLocation.getGeometry().getLocation().getLat();
        double lng = mLocation.getGeometry().getLocation().getLng();
        LatLng latLng = new LatLng(lat, lng);
        addMarker(latLng).setTitle(mLocation.getName());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

    }
}
