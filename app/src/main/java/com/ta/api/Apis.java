package com.ta.api;


import com.ta.models.general.ApiResp;
import com.ta.models.respmodels.Location;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by rahil on 9/9/15.
 */
public interface Apis {


//    @Multipart
//    @POST(ApiConstants.UPDATE_PROFILE)
//    Call<ObjResp<UpdateProfileResponse>> updateProfileApi(@Part("image\"; filename=\"image.jpg") RequestBody file,
//                                                          @Part(ApiConstants.MULTIPART_KEY_JSON) UpdateProfileReq req);



  /*  //for list Response
    @FormUrlEncoded
    @POST("api_name")
    Call<ApiResp<List<GeneralResp>>> getListResp();

    //for object response
    @FormUrlEncoded
    @POST("api_name")
    Call<ApiResp<GeneralResp>> getObjectResp();
*/


    @GET
    Call<ApiResp<List<Location>>> getLocations(@Url String url);

}

