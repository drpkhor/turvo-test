package com.ta.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ta.R;
import com.ta.interfaces.OnItemsClickListener;
import com.ta.models.respmodels.Location;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by rahil on 29/5/17.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private List<Location> mLocations = new ArrayList<>();
    private OnItemsClickListener mListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.ivPlace.setImageURI(mLocations.get(position).getIcon());
        holder.tvName.setText(mLocations.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    public void addAll(List<Location> location) {
        if (location == null || location.isEmpty()) return;
        int prevSize = mLocations.size();
        int count = location.size();
        mLocations.addAll(location);
        notifyItemRangeInserted(prevSize, count);
    }

    public void setOnItemClickListener(OnItemsClickListener listener) {
        mListener = listener;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private SimpleDraweeView ivPlace;
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivPlace = (SimpleDraweeView) itemView.findViewById(R.id.ivPlace);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) mListener.onItemsClick(v, getAdapterPosition());
        }
    }
}
