package com.ta.fragment;


import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ta.R;
import com.ta.base.BaseFragment;
import com.ta.databinding.FragmentMapBinding;
import com.ta.models.events.LocationEvent;
import com.ta.models.events.ZoomToLatLngEvent;
import com.ta.models.respmodels.Location;
import com.ta.utils.Lg;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BaseFragment implements OnMapReadyCallback {


    private FragmentMapBinding mBinding;
    private LatLngBounds.Builder mBuilder;
    private GoogleMap mMap;
    private LocationEvent mEvent;
    private boolean isAlreadyDrawn;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void initializeUi(ViewDataBinding binding) {

        mBinding = (FragmentMapBinding) binding;
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_map;
    }

    public static Fragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        drawMap();

    }

    private void drawMap() {
        if (mMap == null || mEvent == null || isAlreadyDrawn) return;
        isAlreadyDrawn = true;
        mBuilder = new LatLngBounds.Builder();
        for (Location location : mEvent.getLocation()) {

            double lat = location.getGeometry().getLocation().getLat();
            double lng = location.getGeometry().getLocation().getLng();

            LatLng latLong = new LatLng(lat, lng);
            Marker marker = addMarker(latLong);
            marker.setTitle(location.getName());
            mBuilder.include(latLong);
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.12); // offset from edges of the map 12% of screen
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(mBuilder.build(), padding);
        // end of new code
        mMap.animateCamera(cu);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }


    @Subscribe
    public void onEvent(LocationEvent event) {

        mEvent = event;
        drawMap();

    }


    public Marker addMarker(LatLng latLng) {
        //final LatLng latLng = new LatLng(lat, lng);


        MarkerOptions mMarkerOptionsPin = new MarkerOptions().position(latLng);

        Marker marker = mMap.addMarker(mMarkerOptionsPin);

        Lg.d(TAG + "marker id", marker.getId());
        return marker;
    }

    @Subscribe
    public void onEvent(ZoomToLatLngEvent event){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(event.getLatLng(), 18));
    }
}
