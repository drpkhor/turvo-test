package com.ta.fragment;


import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.ta.R;
import com.ta.activities.DetailActivity;
import com.ta.adapter.PlaceAdapter;
import com.ta.base.BaseFragment;
import com.ta.databinding.FragmentListBinding;
import com.ta.interfaces.OnItemsClickListener;
import com.ta.models.events.LocationEvent;
import com.ta.models.events.ZoomToLatLngEvent;
import com.ta.models.respmodels.Location;
import com.ta.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceListFragment extends BaseFragment implements OnItemsClickListener {


    public static final int REQ_CODE_DETAILS = 321;
    private FragmentListBinding mBinding;
    private PlaceAdapter mAdapter;
    private List<Location> mLocations;

    public PlaceListFragment() {
        // Required empty public constructor
    }

    @Override
    public void initializeUi(ViewDataBinding binding) {

        mBinding = (FragmentListBinding) binding;
        mBinding.rvPlaces.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new PlaceAdapter();
        mAdapter.setOnItemClickListener(this);
        mBinding.rvPlaces.setAdapter(mAdapter);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Subscribe
    public void onEvent(LocationEvent event) {
        mLocations = event.getLocation();
        mAdapter.addAll(event.getLocation());
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_list;
    }

    public static Fragment newInstance() {
        return new PlaceListFragment();
    }

    @Override
    public void onItemsClick(View item, int pos) {

        Location.GeometryEntity.LocationEntity location = mLocations.get(pos).getGeometry().getLocation();
        EventBus.getDefault().post(new ZoomToLatLngEvent(new LatLng(location.getLat(), location.getLng())));
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra(AppConstants.LOCATION_DATA_EXTRA, mLocations.get(pos));
        getActivity().startActivityForResult(intent, REQ_CODE_DETAILS);

    }
}
